﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaserProject.Migrations
{
    public partial class changeproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Projectes_ProjectsId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Projectes");

            migrationBuilder.RenameColumn(
                name: "ProjectsId",
                table: "AspNetUsers",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_ProjectsId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_ProjectId");

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NameClient = table.Column<string>(nullable: true),
                    NameSupplier = table.Column<string>(nullable: true),
                    DateStartProject = table.Column<DateTime>(nullable: false),
                    DateEndProject = table.Column<DateTime>(nullable: false),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Projects_ProjectId",
                table: "AspNetUsers",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Projects_ProjectId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "AspNetUsers",
                newName: "ProjectsId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_ProjectId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_ProjectsId");

            migrationBuilder.CreateTable(
                name: "Projectes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    DateEndProjact = table.Column<DateTime>(nullable: false),
                    DateStartProject = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NameClient = table.Column<string>(nullable: true),
                    NameSupplier = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projectes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Projectes_ProjectsId",
                table: "AspNetUsers",
                column: "ProjectsId",
                principalTable: "Projectes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

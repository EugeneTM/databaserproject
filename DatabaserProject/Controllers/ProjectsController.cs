using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DatabaserProject.Date;
using DatabaserProject.Models;
using DatabaserProject.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DatabaserProject.Controllers {
    public class ProjectsController : Controller {

        private ProjectRepository _projectRepository;
        private UserRepository _userRepository;
        private AppDbContext _context;

        public ProjectsController (AppDbContext context) {
            _context = context;
            _projectRepository = new ProjectRepository (_context);
            _userRepository = new UserRepository (_context);
        }

        public IActionResult Index () {
            var projects = _projectRepository.AllProjects ();
            return View (projects);
        }
        public IActionResult Details (string id) {
            var project = _projectRepository.GetSingleProject (id);
            return View (project);
        }

        [HttpGet]
        public IActionResult NewProject () {
            ViewBag.IsEditMode = "false";
            ViewData["Employee"] = new SelectList (_userRepository.AllUsers (), "Id", "FirstName");
            var project = new Project ();
            return View (project);
        }

        [HttpPost]
        public IActionResult NewProject (Project project, string IsEditMode, IFormFile file) {
            if (IsEditMode.Equals ("false")) {
                project.ImageUrl = UploadFile (file, project.Id);
                _projectRepository.Create (project);

            } else {
                var projectId = _projectRepository.GetSingleProject (project.Id);
                projectId.Name = project.Name;
                projectId.NameClient = project.NameClient;
                projectId.NameSupplier = project.NameSupplier;
                projectId.DateStartProject = project.DateStartProject;
                projectId.DateEndProject = project.DateEndProject;
                projectId.Status = project.Status;
                _projectRepository.Edit (projectId);
            }

            return View ();
        }

        [HttpGet]
        public IActionResult Edit (string Id) {
            ViewBag.IsEditMode = "true";
            var projects = _projectRepository.GetSingleProject (Id);
            return View ("NewProject", projects);
        }

        public IActionResult Delete (string id) {
            var project = _projectRepository.GetSingleProject (id);
            _projectRepository.Delete (project);
            return View ();
        }

        public string UploadFile (IFormFile file, string id) {
            var fileName = file.FileName;
            var path = Path.Combine (Directory.GetCurrentDirectory (), "wwwroot/Images", fileName);
            using (var fileStream = new FileStream (path, FileMode.Create)) {
                file.CopyTo (fileStream);
            }

            // var project = _projectRepository.GetSingleProject (id);
            // project.ImageUrl = fileName;
            // _projectRepository.Edit (project);
            return fileName;
        }

    }
}
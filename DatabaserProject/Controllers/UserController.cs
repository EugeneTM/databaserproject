using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatabaserProject.Date;
using DatabaserProject.Models;
using DatabaserProject.Repository;
using Microsoft.AspNetCore.Mvc;

namespace DatabaserProject.Controllers {
    public class UserController : Controller {

        private UserRepository _userRepository;
        private AppDbContext _context;

        public UserController (AppDbContext context) {
            _context = context;
            _userRepository = new UserRepository (_context);
        }

        public IActionResult Index () {
            var users = _userRepository.AllUsers ();
            return View (users);
        }
        public IActionResult Details (string id) {
            var user = _userRepository.GetSingleUser (id);
            return View (user);
        }

        [HttpGet]
        public IActionResult NewUser () {
            ViewBag.IsEditMode = "false";
            var user = new User ();
            return View (user);
        }

        [HttpPost]
        public IActionResult NewUser (User user, string IsEditMode) {

            if (IsEditMode.Equals ("false")) {
                _userRepository.Create (user);
            } else {
                var userId = _userRepository.GetSingleUser (user.Id);
                userId.FirstName = user.FirstName;
                userId.FamilyName = user.FamilyName;
                userId.LastName = user.LastName;
                userId.Email = user.Email;
                _userRepository.Edit (userId);
            }
            return RedirectToAction (nameof (Index));
        }

        [HttpGet]
        public IActionResult Edit (string Id) {
            ViewBag.IsEditMode = "true";
            var user = _userRepository.GetSingleUser (Id);
            return View ("NewUser", user);
        }

        public IActionResult Delete (string id) {
            var user = _userRepository.GetSingleUser (id);
            _userRepository.Delete (user);
            return View ();
        }

    }
}
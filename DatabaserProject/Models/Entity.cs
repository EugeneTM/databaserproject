using System;

namespace DatabaserProject.Models
{
    public class Entity
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
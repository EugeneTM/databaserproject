using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DatabaserProject.Models {
    public class Project : Entity {
        public string Name { get; set; }
        public string NameClient { get; set; }
        public string NameSupplier { get; set; }
        public DateTime DateStartProject { get; set; } = DateTime.Now;
        public DateTime DateEndProject { get; set; }
        public string EmployeeId { get; set; }
        public User Employee { get; set; }
        public IEnumerable<User> Employees { get; set; }

        [Range (0, 5, ErrorMessage = "Значение должно быть 0 >= 5")]
        public int Status { get; set; }
        public string  ImageUrl { get; set; }
    }
}
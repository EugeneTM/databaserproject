using Microsoft.AspNetCore.Identity;

namespace DatabaserProject.Models {
    
    public class User : IdentityUser {
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FamilyName { get; set; }
    }
}
using DatabaserProject.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.CompilerServices;

namespace DatabaserProject.Date {
    public class AppDbContext : IdentityDbContext<User> {

        public DbSet<Project> Projects { get; set; }
        public AppDbContext (DbContextOptions<AppDbContext> options) : base (options) { }
    }
}
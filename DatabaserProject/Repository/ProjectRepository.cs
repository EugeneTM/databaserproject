using System.Collections.Generic;
using System.Linq;
using DatabaserProject.Date;
using DatabaserProject.Models;

namespace DatabaserProject.Repository {
    public class ProjectRepository : IRepositoryProject {
        private AppDbContext _context;

        public ProjectRepository (AppDbContext context) {
            _context = context;
        }
        public List<Project> AllProjects () {

            return _context.Projects.ToList ();
        }

        public void Create (Project projects) {
            _context.Projects.Add (projects);
            _context.SaveChangesAsync ();
        }

        public void Delete (Project projects) {
            _context.Projects.Remove (projects);
            _context.SaveChangesAsync ();
        }

        public void Edit (Project projects) {
            _context.Projects.Update (projects);
            _context.SaveChangesAsync ();
        }

        public Project GetSingleProject (string id) {
            var project = _context.Projects.FirstOrDefault (p => p.Id == id);
            return project;
        }
    }
}
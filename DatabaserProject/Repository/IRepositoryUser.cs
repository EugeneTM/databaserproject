using System.Collections.Generic;
using DatabaserProject.Models;

namespace DatabaserProject.Repository {
    public interface IRepositoryUser {
        void Create (User user);
        void Edit (User user);
        void Delete (User user);
        User GetSingleUser (string id);
        List<User> AllUsers ();
    }
}
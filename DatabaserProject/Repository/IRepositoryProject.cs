using System.Collections.Generic;
using DatabaserProject.Models;

namespace DatabaserProject.Repository {
    public interface IRepositoryProject {
        void Create (Project projects);
        void Edit (Project projects);
        void Delete (Project projects);
        Project GetSingleProject (string id);
        List<Project> AllProjects ();
    }
}
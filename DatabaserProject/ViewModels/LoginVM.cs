using System.ComponentModel.DataAnnotations;

namespace DatabaserProject.ViewModels {
    public class LoginVM {
        
        [Required]
        [Display (Name = "Логин")]
        public string UserName { get; set; }

        [Required]
        [DataType (DataType.Password)]
        [Display (Name = "Пароль")]
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}
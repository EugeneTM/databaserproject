namespace DatabaserProject.ViewModels {
    public class UserVM {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FamilyName { get; set; }
        public string Email { get; set; }

    }
}